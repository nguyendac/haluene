window.addEventListener('DOMContentLoaded', function() {

})
jQuery(function($) {
  $('.single-item').slick({
    autoplay: true,
  });
  $('.center-item').slick({
    infinite: true,
    dots: false,
    slidesToShow: 1,
    centerMode: false,
    autoplay: true,
  });
});
window.onload = function() {
  document.getElementById('home_slider_loading').style.display = 'none';
  document.getElementById('home_slider').style.display = 'block';
}