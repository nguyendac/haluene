<div id="nav_scroll" class="nav_scroll">
  <div class="inner">
    <div class="logo_sr">
      <a href="/"><img src="/gas/common/images/logo_gas_pc.png" alt="Logo gas"></a>
    </div>
    <nav>
      <ul>
        <li><a href="/gas/about" class="icon_fire"><span>ハルエネガスとは</span></a></li>
        <li><a href="/gas/price" class="icon_yen"><span>料金</span></a></li>
        <li><a href="/gas/#subscriber" class="icon_pencil anchor"><span>ガスご加入者様</span></a></li>
        <li><a href="/gas/#emergency" class="icon_plus anchor"><span>緊急対応</span></a></li>
        <li><a href="/inquiry" class="icon_note"><span>ウェブ申し込み</span></a></li>
        <li><a href="/inquiry" class="icon_envelop"><span>お問い合わせ</span></a></li>
        <li id="nav_scroll_mypage" class="nav_scroll_mypage"><a href="/mypage/login.php" target="_blank">マイページ</a></li>
      </ul>
    </nav>
  </div>
</div>
<div class="header_top">
  <div class="header_top_row row">
    <h1><a href="/"><img src="/gas/common/images/logo.png" alt="haluene"></a></h1>
    <div class="header_top_r">
      <div class="header_top_r_b">
        <a href="/mypage/login.php" class="_person"><span>マイページ</span></a><a href="/faq" class="_mess"><span>よくある質問</span></a>
      </div>
      <div class="header_top_r_s">
        <div class="header_top_r_size">
          <p>文字サイズ</p>
          <ul id="size">
            <li><a href="" data-size="small">大</a></li>
            <li><a href="" data-size="medium">中</a></li>
            <li><a href="" data-size="large">小</a></li>
          </ul>
        </div>
        <div class="header_top_r_search">
          <input type="text" placeholder="検索キーワード">
        </div>
      </div>
    </div>
  </div>
</div>
<div class="header_middle row">
  <div class="header_middle_link">
    <ul>
      <li class="_electric"><a href="/"><span>ハルエネでんき</span></a></li>
      <li class="_gas"><a href="/gas"><span>ハルエネガス</span></a></li>
    </ul>
  </div>
  <div class="header_middle_contact">
    <a href="tel:0570-001-296" class="header_middle_contact_phone">0570-001-296</a>
    <span class="header_middle_contact_time">24時間<br>365日</span>
  </div>
</div>
<nav class="header_nav">
  <ul class="row">
    <li><a href="/gas/about" class="icon_fire"><span>ハルエネガスとは</span></a></li>
    <li><a href="/gas/price" class="icon_yen"><span>料金</span></a></li>
    <li><a href="/gas/#subscriber" class="icon_pencil anchor"><span>ガスご加入者様</span></a></li>
    <li><a href="/gas/#emergency" class="icon_plus anchor"><span>緊急対応</span></a></li>
    <li><a href="/inquiry" class="icon_note"><span>ウェブ申し込み</span></a></li>
    <li><a href="/inquiry" class="icon_envelop"><span>お問い合わせ</span></a></li>
  </ul>
</nav>