window.requestAnimFrame = (function() {
  return window.requestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    window.oRequestAnimationFrame ||
    window.msRequestAnimationFrame ||
    function(callback) {
      window.setTimeout(callback, 1000 / 60);
    };
})();
window.cancelAnimFrame = (function() {
  return window.cancelAnimationFrame ||
    window.cancelRequestAnimationFrame ||
    window.webkitCancelAnimationFrame ||
    window.webkitCancelRequestAnimationFrame ||
    window.mozCancelAnimationFrame ||
    window.mozCancelRequestAnimationFrame ||
    window.msCancelAnimationFrame ||
    window.msCancelRequestAnimationFrame ||
    window.oCancelAnimationFrame ||
    window.oCancelRequestAnimationFrame ||
    function(id) { window.clearTimeout(id); };
})();
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
function closest(el, selector) {
  // type el -> Object
  // type select -> String
  var matchesFn;
  // find vendor prefix
  ['matches', 'webkitMatchesSelector', 'mozMatchesSelector', 'msMatchesSelector', 'oMatchesSelector'].some(function(fn) {
    if (typeof document.body[fn] == 'function') {
      matchesFn = fn;
      return true;
    }
    return false;
  })
  var parent;
  // traverse parents
  while (el) {
    parent = el.parentElement;
    if (parent && parent[matchesFn](selector)) {
      return parent;
    }
    el = parent;
  }
  return null;
}

function getCssProperty(elem, property) {
  return window.getComputedStyle(elem, null).getPropertyValue(property);
}
var transform = ["transform", "msTransform", "webkitTransform", "mozTransform", "oTransform"];
var flex = ['-webkit-box', '-moz-box', '-ms-flexbox', '-webkit-flex', 'flex'];
var fd = ['flexDirection', '-webkit-flexDirection', '-moz-flexDirection'];
var animatriondelay = ["animationDelay","-moz-animationDelay","-wekit-animationDelay"];
function getSupportedPropertyName(properties) {
  for (var i = 0; i < properties.length; i++) {
    if (typeof document.body.style[properties[i]] != "undefined") {
      return properties[i];
    }
  }
  return null;
}
var transformProperty = getSupportedPropertyName(transform);
var flexProperty = getSupportedPropertyName(flex);
var fdProperty = getSupportedPropertyName(fd);
var ad = getSupportedPropertyName(animatriondelay);
var easingEquations = {
  easeOutSine: function(pos) {
    return Math.sin(pos * (Math.PI / 2));
  },
  easeInOutSine: function(pos) {
    return (-0.5 * (Math.cos(Math.PI * pos) - 1));
  },
  easeInOutQuint: function(pos) {
    if ((pos /= 0.5) < 1) {
      return 0.5 * Math.pow(pos, 5);
    }
    return 0.5 * (Math.pow((pos - 2), 5) + 2);
  }
};

function isPartiallyVisible(el) {
  var elementBoundary = el.getBoundingClientRect();
  var top = elementBoundary.top;
  var bottom = elementBoundary.bottom;
  var height = elementBoundary.height;
  return ((top + height >= 0) && (height + window.innerHeight >= bottom));
}

function isFullyVisible(el) {
  var elementBoundary = el.getBoundingClientRect();
  var top = elementBoundary.top;
  var bottom = elementBoundary.bottom;
  return ((top >= 0) && (bottom <= window.innerHeight));
}

function CreateElementWithClass(elementName, className) {
  var el = document.createElement(elementName);
  el.className = className;
  return el;
}

function createElementWithId(elementName, idName) {
  var el = document.createElement(elementName);
  el.id = idName;
  return el;
}

function getScrollbarWidth() {
  var outer = document.createElement("div");
  outer.style.visibility = "hidden";
  outer.style.width = "100px";
  document.body.appendChild(outer);
  var widthNoScroll = outer.offsetWidth;
  // force scrollbars
  outer.style.overflow = "scroll";
  // add innerdiv
  var inner = document.createElement("div");
  inner.style.width = "100%";
  outer.appendChild(inner);
  var widthWithScroll = inner.offsetWidth;
  // remove divs
  outer.parentNode.removeChild(outer);
  return widthNoScroll - widthWithScroll;
}

function insertAfter(referenceNode, newNode) {
  referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}
var wordsToArray = function wordsToArray(str) {return str.split('').map(function (e) {return e === ' ' ? '&nbsp;' : e;});};

function insertSpan(elem, letters, startTime) {
  elem.textContent = ''
  var curr = 0
  var delay = 20
  letters.forEach(function(letter,i){
    var span = document.createElement('span');
    span.classList.add('waveText');
    span.innerHTML = letter
    span.style[ad] = (++curr / delay + (startTime || 0)) + 's'
    elem.appendChild(span)
  })
}

if ((navigator.userAgent.indexOf('iPhone') > 0 && navigator.userAgent.indexOf('iPad') == -1) || navigator.userAgent.indexOf('iPod') > 0 || (navigator.userAgent.indexOf('Android') > 0 && navigator.userAgent.indexOf('Mobile') > 0)) {
  var url = window.location.pathname;
  location.href = '/sp'+url;
}
window.addEventListener('DOMContentLoaded',function(){
 new Size();
 new Nc();
 new Anchor();
})
var Nc = (function(){
  function Nc(){
    var n = this;
    this._target = document.getElementById('nav_scroll');
    this._header = document.getElementById('header');
    this._flag = this._header.clientHeight;
    this.handling = function(){
      var _top  = document.documentElement.scrollTop || document.body.scrollTop;
      if(_top > n._flag) {
        n._target.classList.add('fixed');
      } else {
        n._target.classList.remove('fixed');
      }
    }
    window.addEventListener('scroll',n.handling,false);
  }
  return Nc;
})()
var Size = (function() {
  function Size() {
    var s = this;
    this.target = document.getElementById('size');
    this.setCookie = function(cname, val) {
      document.cookie = cname + "=" + val + ";path=/";
    }
    this.getCookie = function(cname) {
      var name = cname + "=";
      var ca = document.cookie.split(';');
      for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
          c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
          return c.substring(name.length, c.length);
        }
      }
      return "";
    }
    this.changeCookie = function(cname, val) {
      s.setCookie(cname, val);
    }
    this.data_size = s.getCookie('data-size') ? s.getCookie('data-size') : 'medium';
    document.documentElement.setAttribute('data-size', s.data_size);
    Array.prototype.forEach.call(s.target.querySelectorAll('li'), function(el) {
      if (el.querySelector('a').getAttribute('data-size') == s.data_size) {
        el.querySelector('a').classList.add('active');
      } else {
        el.querySelector('a').classList.remove('active');
      }
      el.querySelector('a').addEventListener('click', function(e) {
        e.preventDefault();
        if (s.data_size) {
          s.changeCookie('data-size', this.getAttribute('data-size'));
        } else {
          s.setCookie('data-size', this.getAttribute('data-size'));
        }
        this.classList.add('active');
        document.documentElement.setAttribute('data-size', this.getAttribute('data-size'));
        Array.prototype.filter.call(el.parentNode.children, function(child) {
          if (el != child) {
            child.querySelector('a').classList.remove('active');
          }
        })
      })
    })
  }
  return Size;
})()
var Anchor = (function(){
  function Anchor(){
    var a = this;
    this._target = '.anchor';
    this._header = document.getElementById('nav_scroll');
    this.timer;
    this.flag_start = false;
    this.iteration;
    this.eles = document.querySelectorAll(this._target);
    this.stopEverything = function(){
      a.flag_start = false;
    }
    this._getbuffer = function() {
      var _buffer;
      _buffer = a._header.clientHeight;
      return _buffer;
    }
    this._buffer = this._getbuffer();
    this.scrollToY = function(scrollTargetY,speed,easing){
      var scrollY = window.scrollY || window.pageYOffset,
        scrollTargetY = scrollTargetY || 0,
        speed = speed || 2000,
        easing = easing || 'easeOutSine',
        currentTime = 0;
      var time = Math.max(.1, Math.min(Math.abs(scrollY - scrollTargetY) / speed, .8));
      function tick() {
        if(a.flag_start){
          currentTime += 1 / 60;
          var p = currentTime / time;
          var t = easingEquations[easing](p);
          if (p < 1) {
            requestAnimFrame(tick);
            window.scrollTo(0, scrollY + ((scrollTargetY - scrollY) * t));
          } else {
            window.scrollTo(0, scrollTargetY);
          }
        }
      }
      tick();
    }
    Array.prototype.forEach.call(this.eles,function(el,i){
      el.addEventListener('click',function(e){
        var next = el.getAttribute('href').split('#')[1];
        if(document.getElementById(next)){
          a.flag_start = true;
          e.preventDefault();
          a.scrollToY((document.getElementById(next).offsetTop-a._buffer),1500,'easeOutSine');
        }
      })
    });
    this._start = function(){
      var next = window.location.hash.split('#')[1];
      a.flag_start = true;
      if(next){
        a.scrollToY((document.getElementById(next).offsetTop - a._buffer),1500,'easeOutSine');
      }
    }
    window.addEventListener('load',a._start,false);
    document.querySelector("body").addEventListener('mousewheel',a.stopEverything,false);
    document.querySelector("body").addEventListener('DOMMouseScroll',a.stopEverything,false);
  }
  return Anchor;
})()