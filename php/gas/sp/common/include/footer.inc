<div class="ft_top">
  <ul class="ft_top_l row">
    <li>
      <a href="https://ondankataisaku.env.go.jp/coolchoice/"><img src="/gas/sp/common/images/coolchoice.png" alt="banner cool choice"></a>
    </li>
    <li>
      <a href="https://s-heart.org/"><img src="/gas/sp/common/images/sport.png" alt="banner port"></a>
    </li>
  </ul>
  <!--/.list-->
</div>
<!--/.ft_top-->
<div class="ft_easy show_sp">
  <div class="row">
    <figure>
      <img src="/gas/sp/common/images/sun_ft_02.png" alt="sun">
    </figure>
    <div class="ft_easy_btn">
      <p>24時間受付中</p>
      <div class="ft_easy_btn_w">
        <a href="#"><em><small>かんたん</small>ウェブ申し込み</em></a>
      </div>
    </div>
  </div>
</div>
<!--/.ft_easy-->
<div class="ft_ct">
  <div class="row">
    <figure class="show_pc">
      <img src="/gas/sp/common/images/sun_ft.png" alt="sun">
    </figure>
    <p class="ft_ct_txt">お申し込み
      <br class="show_pc">お問い合わせは
      <br class="show_pc">お気軽に</p>
    <div class="ft_ct_tel">
      <a href="tel:0570-001-296">0570-001-296</a>
      <em><span>営業時間</span><span>24時間 365日</span></em>
    </div>
    <!--/.ft_ct_tel-->
    <div class="ft_ct_contact">
      <a href="/inquiry/"><span>お問い合わせ</span></a>
    </div>
    <!--/.contact-->
  </div>
</div>
<!--/.ft_ct-->
<div class="ft_nav">
  <div class="row">
    <div class="ft_nav_l">
      <dl>
        <dd>
          <ul>
            <li><a href="/">ハルエネでんき HOME</a></li>
            <li class="show_pc"><a href="/about/">ハルエネでんきとは</a></li>
            <li class="show_pc"><a href="/price/">料金</a></li>
            <li class="show_pc"><a href="/case/">導入事例</a></li>
            <li class="show_pc"><a href="/flow/">お申し込みの流れ</a></li>
            <li class="show_pc"><a href="/option/">オプション</a></li>
            <li class="show_pc"><a href="/sosei/">地域創生プロジェクト</a></li>
            <li class="show_pc"><a href="/ouen/">業界応援プロジェクト</a></li>
          </ul>
        </dd>
      </dl>
      <dl>
        <dt class="show_pc"><img src="/gas/sp/common/images/logo_gas_sp.png" alt="denki"></dt>
        <dd>
          <ul>
            <li><a href="/gas/#">ハルエネガス　HOME</a></li>
            <li class="show_pc"><a href="/gas/about/">ハルエネガスとは</a></li>
            <li class="show_pc"><a href="/gas/price/">ハルエネ料金表</a></li>
            <li class="show_pc"><a href="/gas/のガスご加入社さまのみなさまの部分に飛ぶ">ガスご加入社のみなさま</a></li>
            <li class="show_pc"><a href="/gas/のガスの緊急対応はこちらの部分に飛ぶ">ガスの緊急対応</a></li>
          </ul>
        </dd>
      </dl>
      <dl>
        <dt class="show_pc">共通</dt>
        <dd>
          <ul>
            <li><a href="/information/">ニュース・プレスリリース</a></li>
            <li class="show_sp"><a href="/faq/">よくある質問</a></li>
            <li><a href="/yakkan_list/">電気供給約款・重要事項説明書</a></li>
            <li><a href="/haluo/">ハルオくんの部屋</a></li>
            <li><a href="/declaration/">反社会的勢力排除宣言</a></li>
            <li><a href="/partner/">事業パートナー様募集</a></li>
            <li><a href="/company/">会社概要</a></li>
            <li><a href="/inquiry/">お問い合わせ</a></li>
            <li><a href="/apply/">ウェブ申し込み</a></li>
            <li class="show_pc"><a href="/faq/">よくある質問</a></li>
          </ul>
        </dd>
      </dl>
    </div>
    <!--/.ft_nav_l-->
    <div class="ft_nav_s">
      <ul class="footer_sns_link">
        <li><a href="https://www.facebook.com/haluene/" target_text="_blank"><img src="https://www.haluene.co.jp/wp-content/themes/haluene_sp/images/icon_facebook.png" alt="facebook"></a></li>
        <li><a href="https://twitter.com/HALUENEofficial" target_text="_blank"><img src="https://www.haluene.co.jp/wp-content/themes/haluene_sp/images/Twitter_sp.png" alt="Twitter"></a></li>
        <li><a href="https://privacymark.jp" target_text="_blank"><img src="https://www.haluene.co.jp/wp-content/themes/haluene_sp/images/privacy.png" alt="プライバシーマーク"></a></li>
      </ul>
    </div>
    <!--/.ft_nav_s-->
    <div class="ft_nav_r show_pc">
      <ul>
        <li><a href="https://privacymark.jp/"><img src="/gas/sp/common/images/privacy.png" alt="Privacy"></a></li>
        <li><a href="https://twitter.com/HALUENEofficial"><img src="/gas/sp/common/images/Twitter_pc.png" alt="Twitter"></a></li>
      </ul>
      <div class="ft_nav_fb">
        <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fhaluenedenki%2F%3Fref%3Dbookmarks&amp;tabs=timeline&amp;width=235&amp;height=200&amp;small_header=false&amp;adapt_container_width=true&amp;hide_cover=false&amp;show_facepile=true&amp;appId" width="235" height="200" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true"></iframe>
      </div>
      <!--/.ft_nav_fb-->
    </div>
    <!--/.ft_nav_r-->
  </div>
</div>
<!--/.ft_nav-->
<div class="ft_bottom">
  <div class="row">
    <p class="ft_bottom_copyright">Copyright &copy; 2018 Haluene Co.,Ltd All Right Reserved.</p>
    <div class="ft_bottom_r">
      <ul class="ft_bottom_r_l show_pc">
        <li><a href="/policy/">プライバシーポリシー</a></li>
        <li><a href="/partner/">事業パートナー様募集</a></li>
        <li><a href="/matome/">コラム</a></li>
      </ul>
      <!--/list-->
      <em>REV0003556</em>
    </div>
    <!--/.ft_bottom_r-->
  </div>
</div>
<!--/.ft_bottom-->