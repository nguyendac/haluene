<section id="gas_manage" class="gas_manage common_step row">
  <h2 class="common_step_ttl">古いガス管の管理と注意点</h2>
  <div class="common_step_pad">
    <h3 class="common_step_sub_ttl">古くなったガス管は交換してください</h3>
    <figure class="gas_manage_figure_i">
      <img src="/gas/sp/images/img_01_pc.png" alt="img 01">
      <figcaption>※場所に合わせ最適なガス管にお取り替えします。</figcaption>
    </figure>
    <p class="gas_manage_title">土の中に埋められている白ガス管（亜鉛メッキ鋼管）は、交換しましょう。</p>
    <ul class="gas_manage_list">
      <li>お取り替えをご検討いただきたいのは、お客さまの所有物となる敷地内のガス管です。お取り替えは有償で行わせていただきます。</li>
      <li>現在ガス管の材料として使用されているポリエチレン管や被履鋼管は、腐食や地震に強く、地震対策としても有効です。</li>
      <li>場所に合わせた、最適なご提案をさせていただきますので、導管事業者までお問い合わせください。</li>
    </ul>
    <div class="common_step_note">
      <h4><img src="/gas/sp/images/txt_note_01.png" alt="text note 01"></h4>
      <div class="common_step_note_c _flex">
        <div class="common_step_note_c_txt">鋼管表面に亜鉛メッキを施したガス管をいいます。白ガス管は、強度・耐食性・施工性等を兼ね備えたガス管の材料として昭和20年代から一般的に使用されてきました。長年土の中に埋められた白ガス管は、土の性質や水分などの影響により徐々に腐食が進行し、ガスもれが発生する場合があります。<br><span class="red">※土質や環境により腐食の度合いが異なります。</span></div>
        <figure>
          <img src="/gas/sp/images/gas_manage_img_01.png" alt="">
          <figcaption>&#x25B2; 白ガス管</figcaption>
        </figure>
      </div>
    </div>
    <div class="gas_manage_detail">
      <figure><img src="/gas/sp/images/img_03_pc.png" alt=""></figure>
      <div class="gas_manage_detail_txt">
        <p>&#xFF0A;改装や敷地内を掘る工事をされるときは導管事業者へご連絡ください。</p>
        <p>&#xFF0A;ガスメーターは計量法に基づき、検定満期となる前に導管事業者がお取り替満期となる前に導管事業者がお取り替えいたします。（10年または7年）</p>
      </div>
    </div>
  </div>
</section><!-- end manage gas -->
<section id="gas_house" class="gas_house common_step row">
  <h2 class="common_step_ttl">地震・火災のときの対応と注意点</h2>
  <div class="common_step_pad">
    <h3 class="common_step_sub_ttl">身の安全を最優先に確保してください</h3>
    <div class="gas_house_wrap">
      <article>
        <h4>まずは身の安全を確保しましょう。</h4>
        <p>まずは机の下に身を隠すなどしてください。震度5相当以上の地震の場合は、ガスメーター（マイコンメーター）が自動的にガスをしゃ断します。あわてず落ち着いて行動しましょう。</p>
        <figure><img src="/gas/sp/images/img_04_pc.png" alt=""></figure>
      </article>
      <article>
        <h4>揺れがおさまったら、ガスの火を消してください。</h4>
        <p>ガス機器を使用していた場合、ガス機器のスイッチを止めて、ガス栓を閉めてください。</p>
        <figure><img src="/gas/sp/images/img_05_pc.png" alt=""></figure>
      </article>
    </div>
    <div class="gas_house_sec">
      <h5><img src="/gas/sp/images/house_gas_sec_ttl.png" alt=""></h5>
      <div class="gas_house_sec_wrap">
        <div class="gas_house_sec_col">
          <ul>
            <li>ガス機器周辺でガスの臭いがしないか。</li>
            <li>ガス機器本体に変形や破損などの異常がないか。</li>
            <li>煙突式などの屋内外の給排気設備に異常がないか。<small>（はずれ・へこみ・穴あきがないか目眼で確認してください）</small></li>
          </ul>
          <figure><img src="/gas/sp/images/img_06_pc.png" alt=""></figure>
        </div>
        <div class="gas_house_sec_col">
          <ul>
            <li>ガス接続具が正しく接続されているか。</li>
          </ul>
          <p>（接続具にはずれがないか目視で確認してください）</p>
          <figure><img src="/gas/sp/images/img_07_pc.png" alt=""></figure>
        </div>
      </div>
    </div>
    <div class="common_step_note">
      <h4><img src="/gas/sp/images/text_note_02.png" alt=""></h4>
      <div class="common_step_note_c">
        <ul>
          <li>異常を確認した場合は、<span class="red">火災や一酸化炭素中毒など、事故</span>のおそれがありますので、メーカーや販売店などへ点検・修理を依頼するとともに、導管事業者へご連絡ください。</li>
          <li>ガス機器を使用していて<span class="red">目がチカチカしたり、気分がわるくなったり、不快な臭いがした場合</span>にガス機器の使用を中止し、<span class="red">修理の手配</span>をしてください。</li>
        </ul>
      </div>
    </div>
  </div>
</section><!-- end manage gas -->
<section id="gas_stove" class="gas_stove common_step row">
  <h2 class="common_step_ttl">ガスが止まったときの対応</h2>
  <div class="common_step_pad">
    <h3 class="common_step_sub_ttl">ガスが出ない時は、ガスメータをご確認ください</h3>
    <p class="gas_stove_intro">ご家庭のすべてのガス機器が使えない場合は、ガスメーターの表示ランプを確認してください。点滅している場合は、<span class="red">周囲がガス臭くないことを確認</span>してから復帰の手順を行ってください。万一ガス臭い場合は、復帰の手順を行わず、すぐに導管事業者へ連絡してください。</p>
    <div class="gas_stove_sec_top">
      <figure><img src="/gas/sp/images/img_08_pc.png" alt=""></figure>
      <div class="gas_stove_sec_top_r">
        <p>ガスメーターには、下記のような場合に安全装置がはたらいて自動的にガスを止める機能があります。安全装置がはたらいた場合、表示ランプ（赤）が点滅します。</p>
        <ul>
          <li>大きな地震が発生した場合</li>
          <li>多量にガスがもれた場合</li>
          <li>ガスの圧力が所定の値より低くなった場合</li>
          <li>ガス機器を長時間使用した場合</li>
        </ul>
      </div>
    </div>
    <div class="gas_stove_sec_mid">
      <h4><img src="/gas/sp/images/gas_stove_sec_ttl.png" alt=""></h4>
      <div class="gas_stove_sec_mid_w">
        <section>
          <figure>
            <img src="/gas/sp/images/img_09_pc.png" alt="">
          </figure>
          <div class="_right">
            <h5>器具栓を閉じるか、運転スイッチを切り、すべてのガス機器を止めてください。屋外のガス機器も忘れずに。</h5>
            <p>使っていないガス栓は閉まっていることを確認してください。このときメーターガス栓は閉めないでください。</p>
          </div>
        </section>
        <section>
          <figure>
            <img src="/gas/sp/images/img_10_pc.png" alt="">
          </figure>
          <div class="_right">
            <h5>復帰ボタンのキャップを手で左に回し、キャップをはずしてください。</h5>
          </div>
        </section>
        <section>
          <figure>
            <img src="/gas/sp/images/img_11_pc.png" alt="">
          </figure>
          <div class="_right">
            <h5>復帰ボタンを奥までしっかり押して、表示ランプが点灯したらすぐに手を離す。</h5>
            <p>復帰ボタンが元に戻り、表示ランプが再点滅します。その後、キャップを元に戻しておきます。</p>
          </div>
        </section>
        <section>
          <figure>
            <img src="/gas/sp/images/icon_clock.png" alt="">
          </figure>
          <div class="_right">
            <h5>約3分間お待ちください。</h5>
            <p>この間ガスもれがないか確認していますので、ガスを使わないでください。3分経過後に、再度ガスメーターをご確認いただき、表示ランプの点滅が消えていれば、ガスが使えます。</p>
            <span class="red">＊正常に復帰しない場合や、不明な点がある場合は導管事業者へ連絡してください。</span>
          </div>
        </section>
      </div>
    </div>
    <div class="gas_stove_sec_bot">
      <h4>普段からガスメーターの位置を確認してください</h4>
      <div class="gas_stove_sec_bot_w">
        <figure>
          <img src="/gas/sp/images/img_12_pc.png" alt="">
          <figcaption>マンション・アパートの場合</figcaption>
        </figure>
        <figure>
          <img src="/gas/sp/images/img_13_pc.png" alt="">
          <figcaption>一戸建ての場合</figcaption>
        </figure>
      </div>
    </div>
  </div>
</section><!-- end manage gas -->
<section id="gas_caution" class="gas_caution common_step row">
  <h2 class="common_step_ttl">もしものときの対応と注意点</h2>
  <div class="common_step_pad">
    <section class="gas_caution_sec_smell">
      <h3 class="common_step_sub_ttl">もしもガスが臭いと感じたら</h3>
      <div class="gas_caution_smell">
        <figure>
          <figcaption>室内の<span class="red">火はすべて消し</span>マッチをすったり、タバコを吸わないでください。</figcaption>
          <img src="/gas/sp/images/img_14_pc.png" alt="">
        </figure>
        <figure>
          <figcaption>着火源となるコンセントやスイッチに<span class="red">触れない</span>でください。</figcaption>
          <img src="/gas/sp/images/img_15_pc.png" alt="">
        </figure>
        <figure>
          <figcaption>扉や窓を十分に開けて、<span class="red">風通しをよくして</span>ガスを室外へ追い出しましょう。</figcaption>
          <img src="/gas/sp/images/img_16_pc.png" alt="">
        </figure>
        <figure>
          <figcaption>ガス栓・器具栓を閉めましょう。</figcaption>
          <img src="/gas/sp/images/img_17_pc.png" alt="">
        </figure>
      </div>
      <div class="gas_caution_smell_n">
        <figure><img src="/gas/sp/images/lion_04.png" alt=""></figure>
        <h4><img src="/gas/sp/images/txt_gas_stove_01.png" alt=""></h4>
        <p>ガス警報器が作動したり、ガス臭いと感じたときにはすぐに導管事業者へ連絡し、点検を受けるまではガスを使わないでください。</p>
      </div>
    </section>
    <section class="gas_caution_sec_disater">
      <h3 class="common_step_sub_ttl">災害時の対応</h3>
      <div class="gas_caution_disater_w">
        <article>
          <h5>火災のときは</h5>
          <p>メーターガス栓を閉め、消防署・導管事業者に連絡し、消防署員などに、あとの処理を頼んでください。</p>
          <figure class="_mar"><img src="/gas/sp/images/firetruck.png" alt=""></figure>
        </article>
        <article>
          <h5>避難のときは</h5>
          <p>ガス機器の器具栓やガス栓を閉め、ご近所と協力し合って行動しましょう。高齢者やけが人には手をかしてあげましょう。</p>
          <figure><img src="/gas/sp/images/older.png" alt=""></figure>
        </article>
      </div>
    </section>
    <div class="common_step_note">
      <h4><img src="/gas/sp/images/txt_note_04.png" alt=""></h4>
      <div class="common_step_note_c">
        <ul>
          <li><strong>24時間・365日の保安体制</strong><br>ガスもれ、ガス事故などの<span class="red">緊急時</span>に備えて、<span class="red">係員が待機</span>しています。</li>
          <li><strong>保安点検</strong><br>法令に基づき定期的にお客さま宅にお伺いし、<span class="red">ガスもれ点検や給排気設備の調査</span>を行っています。</li>
        </ul>
      </div>
    </div>
  </div>
</section><!-- end manage gas -->