<section class="st_d_sub">
  <div class="row">
    <h2 class="ttl">ガスご加入者様</h2>
    <ul class="list_anc">
      <li>
        <a href="#">
          <strong>都市ガスガイドBOOK</strong>
          <em>パンフレットをダウンロードいただけます。</em>
        </a>
      </li>
       <li>
        <a href="#gas_manage" class="anchor">
          <strong>古いガス管の<br>管理と注意点</strong>
          <em>古くなったガス管は、交換が必要です。</em>
        </a>
      </li>
       <li>
        <a href="#gas_house" class="anchor">
          <strong>地震・火災のときの<br>対応と注意点</strong>
          <em>地震・火災のときは、身の安全を最優先に確保してください。</em>
        </a>
      </li>
       <li>
        <a href="#gas_stove" class="anchor">
          <strong>ガスが止まった<br>ときの対応</strong>
          <em>ガスが出ないときは、ガスメーターをご確認ください。</em>
        </a>
      </li>
       <li>
        <a href="#gas_caution" class="anchor">
          <strong>もしものときの<br>対応と注意点</strong>
          <em>ガス臭いと感じた時は、すぐに導管事業者へ連絡してください。</em>
        </a>
      </li>
    </ul>
    <!--/.list_anc-->
    <dl class="des_tel">
      <dt>ガスの緊急対応はこちら</dt>
      <dd>
        <h3>東京ガス(導管事業者)</h3>
        <article>
          <h4><img src="/gas/sp/images/txt_art_01.png" alt="Title art 01"></h4>
          <h5 class="ttl_tbl">●ガス臭いと感じたら、すぐに下記へご連絡ください！</h5>
          <p class="txt_org">
            <span class="txt_org_f">ガス漏れ通報専用電話</span>
            <span>受付時間：24時間<small>(ガス漏れ通報専用・無休)</small></span>
          </p>
          <table class="tbl_tel_01">
            <thead>
              <tr>
                <td>
                  <p>
                    <span>東京ガス 緊急保安受付窓口</span>
                    (ナビダイヤル)※必ず「0」からダイヤルしてください。
                  </p>
                  <a class="tel tel_01" href="tel:0570-002299"><img src="/gas/sp/images/tel_01.png" alt="Tel 01"></a>
                </td>
              </tr>
              <tr>
                <td>
                  <p>
                    <span>東京ガス 緊急保安受付窓口</span>
                    (ナビダイヤル)※必ず「0」からダイヤルしてください。
                  </p>
                  <a class="tel tel_02" href="tel:03-6735-8899"><img src="/gas/sp/images/tel_02.png" alt="Tel 02"></a>
                </td>
              </tr>
              <tr>
                <td colspan="2">
                  <div class="dt_flex">
                    <p>
                      佐倉市・成田市・八千代市・千葉市・四街道市・<br>
                      富里市・酒々井町・芝山町・多古町に<br>
                      お住まいのお客さま(※旧千葉ガス供給エリア)
                    </p>
                    <strong>佐倉支社</strong>
                    <a class="tel tel_03" href="tel:043-483-2280"><img src="/gas/sp/images/tel_03.png" alt="Tel 03"></a>
                  </div>
                  <!--/.dt-flex-->
                </td>
              </tr>
              <tr>
                <td colspan="2">
                  <div class="dt_flex">
                    <p>
                      つくば市・つくばみらい市にお住まいのお客さま<br>
                      (※旧筑波学園ガス供給エリア)
                    </p>
                    <strong>つくば支社</strong>
                    <a class="tel tel_04" href="tel:029-848-5151"><img src="/gas/sp/images/tel_04.png" alt="Tel 04"></a>
                  </div>
                  <!--/.dt-flex-->
                </td>
              </tr>
            </thead>
          </table>
          <!--/.tbl_tel_01-->
        </article>
        <article>
          <h4><img src="/gas/sp/images/txt_art_02.png" alt="Title art 02"></h4>
          <h5 class="ttl_tbl">●ガス漏れ以外の導管に関するお問い合わせは(内管工事等)</h5>
          <table class="tbl_tel_02">
            <thead>
              <tr>
                <td class="bx_t">
                  <span class="bkg_grey">受付時間</span>
                  <em class="time"><span>月曜日〜土曜日</span><span>9:00~17:00</span></em>
                  <em class="time"><span>日曜日・祝日</span><span>9:00~17:00</span></em>
                </td>
              </tr>
              <tr>
                <td class="bx_center">
                  <p>
                    <span>東京ガスお客さまセンター(総合)</span>
                    (ナビダイヤル)※必ず「0」からダイヤルしてください。
                  </p>
                  <a class="tel tel_05" href="tel:0570-002211"><img src="/gas/sp/images/tel_05.png" alt="Tel 05"></a>
                </td>
              </tr>
              <tr>
                <td class="bx_center">
                  <p>
                    <span>ナビダイヤルをご使用になれない場合</span>
                    (IP電話・海外からのご利用など)
                  </p>
                  <a class="tel tel_06" href="tel:03-3344-9100"><img src="/gas/sp/images/tel_06.png" alt="Tel 06"></a>
                </td>
              </tr>
              <tr>
                <td>
                  <p>・上記以外の時間帯は、ガス臭い、ガスが出ないなどの安全に関わる緊急のご用件のみを承っております。ご理解とご協力をお願いいたします。</p>
                  <p>・次の時間帯・曜日・時期は混雑が予想されます。9:00～10:00・日曜日・祝日・引っ越しシーズンとなる3月中旬～4月上旬</p>
                </td>
              </tr>
               <tr>
                <td class="bx_t">
                  <span class="bkg_grey">受付時間</span>
                  <em class="time"><span>月曜日〜土曜日</span><span>8:45~17:30</span></em>
                  <em class="txt_center">(日曜・祝日除く)</em>
                </td>
              </tr>
              <tr>
                <td>
                  <div class="dt_flex">
                    <p>
                      佐倉市・成田市・八千代市・千葉市・四街道市・
                      富里市・酒々井町・芝山町・多古町にお住まいのお客さま
                      (※旧千葉ガス供給エリア)
                    </p>
                    <strong>佐倉支社</strong>
                    <a class="tel tel_07" href="tel:043-483-2280"><img src="/gas/sp/images/tel_07.png" alt="Tel 07"></a>
                  </div>
                  <!--/.dt-flex-->
                </td>
              </tr>
              <tr>
                <td>
                  <div class="dt_flex">
                    <p>
                      つくば市・つくばみらい市にお住まいのお客さま
                      (※旧筑波学園ガス供給エリア)
                    </p>
                    <strong>つくば支社</strong>
                    <a class="tel tel_08" href="tel:029-848-5151"><img src="/gas/sp/images/tel_08.png" alt="Tel 08"></a>
                  </div>
                  <!--/.dt-flex-->
                </td>
              </tr>
            </thead>
          </table>
          <!--/.tbl_tel_02-->
        </article>
      </dd>
    </dl>
  </div>
</section>
<!--/.st_d_sub-->