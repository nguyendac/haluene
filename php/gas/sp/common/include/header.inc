<div class="hd_top">
  <div class="row">
    <p>ガス自由化でコストカット- ハルエネガス</p>
  </div>
</div>
<!--/.hd_top-->
<div class="hd_bottom">
  <div class="row">
    <h1 class="logo">
      <a href="/gas"><img src="/gas/sp/common/images/logo_gas_sp.png" alt="Logo Gas"></a>
    </h1>
    <!--/.logo-->
    <div class="hd_bottom_r">
      <ul class="hd_bottom_r_l">
        <li>
          <a href="/mypage/login.php">マイページ</a>
        </li>
        <li>
          <a href="#" id="open_nav">メニュー</a>
        </li>
      </ul>
    </div>
    <!--/.hd_bottom-->
  </div>
</div>
<!--/.hd_bottom-->
<div id="overlay" class="overlay"></div>
<div id="menu" class="menu">
  <p class="menu_close_w"><a href="" class="menu_close" id="close_nav">閉じる</a></p>
  <div class="menu_btn">
    <a href=""><span>お問い合わせ</span></a>
    <a href=""><span>ウェブ申し込み</span></a>
  </div>
  <ul class="menu_main">
    <li><a href="">ハルエネガスとは</a></li>
    <li><a href="">料金</a></li>
    <li><a href="">ガスご加入者様</a></li>
    <li><a href="">緊急対応</a></li>
  </ul>
  <ul class="menu_extra">
    <li><a href="">お申し込みの<br>流れ</a></li>
    <li><a href="">よくある質問</a></li>
    <li><a href="">プライバシー<br>ポリシー</a></li>
    <li><a href="">会社概要</a></li>
  </ul>
  <div class="menu_pad">
    <p class="menu_phone"><a href="tel:0570-001-296">0570-001-296</a><span>24時間365日</span></p>
    <a href="" class="menu_to_electic"><img src="/gas/sp/common/images/logo_electric.png" alt=""></a>
    <a href="" class="menu_contact"><span>お問い合わせ</span></a>
  </div>
</div>